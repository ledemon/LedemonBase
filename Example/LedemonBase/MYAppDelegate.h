//
//  MYAppDelegate.h
//  LedemonBase
//
//  Created by ledemon on 07/17/2019.
//  Copyright (c) 2019 ledemon. All rights reserved.
//

@import UIKit;

@interface MYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
