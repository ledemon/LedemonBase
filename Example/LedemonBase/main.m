//
//  main.m
//  LedemonBase
//
//  Created by ledemon on 07/17/2019.
//  Copyright (c) 2019 ledemon. All rights reserved.
//

@import UIKit;
#import "MYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MYAppDelegate class]));
    }
}
