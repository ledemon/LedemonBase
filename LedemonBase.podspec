#
# Be sure to run `pod lib lint LedemonBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LedemonBase'
  s.version          = '0.1.0'
  s.summary          = 'A short description of LedemonBase.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/ledemon/LedemonBase'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ledemon' => 'none' }
  s.source           = { :git => 'https://gitee.com/ledemon/LedemonBase.git', :tag => '#{s.version}' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = ['LedemonBase/Classes/**/*.{h,m}','LedemonBase/Classes/*.h']
  
  # s.resource_bundles = {
  #   'LedemonBase' => ['LedemonBase/Assets/*.png']
  # }

   s.public_header_files = 'LedemonBase/Classes/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
