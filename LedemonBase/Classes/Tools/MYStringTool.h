//
//  MYStringTool.h
//  LedemonBase
//
//  Created by YiDian on 2019/7/17.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MYStringTool : NSObject

+(NSAttributedString *)setString:(NSString *)str withColorOneStr:(NSString *)oneStr andColorOne:(UIColor *)colorOne andColorTwoStr:(NSString *)twoStr andColorTwo:(UIColor *)colorTwo;
//获取文字高
+(CGFloat)getHeightWithString:(NSString *)string Font:(CGFloat)font fontName:(NSString *)fontName width:(CGFloat)width;

//获取文字宽
+ (CGFloat)getWidthWithString:(NSString *)test Font:(CGFloat)font fontName:(NSString *)fontName;

//请求的字符串拼接
+(NSString *)stringWithDict:(NSDictionary*)dict;
//
+(NSString *)stringWithArr:(NSArray *)arr;

//时长转string
+(NSString *)stringWithDuration:(NSInteger)duration;

//video 描述
+(NSString *)stringWithchannelName:(NSString *)channelName playNum:(NSString *)playNum createTime:(NSString *)createTime;

+(NSString *)stringWithchannelName:(NSString *)channelName playNum:(NSString *)playNum;

+(NSString *)stringWithloveNum:(NSString *)loveNum;

//playNum
+(NSString *)stringWithplayNum:(NSString *)playNum;
//SubscribeNum
+(NSString *)stringWithSubscribeNum:(NSString *)subscribeNum;

//数字转str    （加万）
+(NSString *)stringWithNum:(NSString *)num;

+(NSString *)stringWithTime:(NSString *)time;

+(NSString *)stringWithcreateTime:(NSString *)createTime;

+(NSString *)dateStringWithcreateTime:(NSString *)createTime;

//返回tvType
+(NSString *)stringWithTvType:(int)tvType;
//返回License
+(NSString *)stringWithLicense:(NSString *)license;

//正则匹配
#pragma 正则匹配手机号
+ (BOOL)checkTelNumber:(NSString *) telNumber;
#pragma 正则匹配手机号/电话
+ (BOOL)checkPhoneNumber:(NSString *) telNumber;
//邮箱
+ (BOOL) checkEmail:(NSString *)email;
#pragma 正则匹配用户密码6-16位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password;
#pragma 正则匹配验证码
+ (BOOL)checkVerCode:(NSString *) verCode;


#pragma 正则匹配用户姓名,20位的中文或英文
+ (BOOL)checkUserName : (NSString *) userName;
#pragma 正则匹配用户身份证号
+ (BOOL)checkUserIdCard: (NSString *) idCard;
#pragma 正则匹配URL
+ (BOOL)checkURL : (NSString *) url;

#pragma 正则匹配标签：中文或英文
+ (BOOL)checkTag : (NSString *) tag;

#pragma 渐变
+(void)TextGradientview:(UIView *)view bgVIew:(UIView *)bgVIew gradientColors:(NSArray *)colors gradientStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

+ (NSString *)translation:(NSString *)arebic;

#pragma mark - 获取这个字符串ASting中的所有abc的所在的index
+ (NSMutableArray *)getRangeStr:(NSString *)text findText:(NSString *)findText;
@end

NS_ASSUME_NONNULL_END
