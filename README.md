# LedemonBase

[![CI Status](https://img.shields.io/travis/ledemon/LedemonBase.svg?style=flat)](https://travis-ci.org/ledemon/LedemonBase)
[![Version](https://img.shields.io/cocoapods/v/LedemonBase.svg?style=flat)](https://cocoapods.org/pods/LedemonBase)
[![License](https://img.shields.io/cocoapods/l/LedemonBase.svg?style=flat)](https://cocoapods.org/pods/LedemonBase)
[![Platform](https://img.shields.io/cocoapods/p/LedemonBase.svg?style=flat)](https://cocoapods.org/pods/LedemonBase)

## 示例

请先安装cocoapods 使用 `pod install` 引入

## 安装

LedemonBase 可通过 [CocoaPods](https://cocoapods.org). 安装
只需将以下行添加到您的Podfile中:
- 头部添加
```ruby
platform:ios,'8.0'
source 'https://gitee.com/ledemon/LedemonBase.git'
```
```ruby
pod 'LedemonBase', :git => "git@gitee.com:ledemon/LedemonBase.git"
```

## 使用
引入
```ObjC
#import <LedemonBase/MYStringTool.h>
```
具体方法请参照 `MYStringTool.h` 注释
## 作者

ledemon, none

## License

LedemonBase is available under the MIT license. See the LICENSE file for more info.
